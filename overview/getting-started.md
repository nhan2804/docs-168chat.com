# Getting Started

After successfully [creating the project](../product-guides/project/create-project.md), go to the project's [Link Embed settings](../product-guides/project/project-config/link-embed.md) page.

## Server Side Rendering

After getting the Link Embed, embed it in the script section of your website.

<pre class="language-html"><code class="lang-html"><strong>#Sample embedded website
</strong><strong>&#x3C;!DOCTYPE html>
</strong>&#x3C;html>
  &#x3C;head>
    &#x3C;meta charset="utf-8" />
    &#x3C;meta http-equiv="X-UA-Compatible" content="IE=edge" />
    &#x3C;title>Page Title&#x3C;/title>
    &#x3C;meta name="viewport" content="width=device-width, initial-scale=1" />
  &#x3C;/head>
  &#x3C;body
    &#x3C;h1>Example Page&#x3C;/h2>
  >&#x3C;/body>
  &#x3C;script
    data-src-embed="https://embed.168livechat.com/"
    data-src-js-embed="https://app.168livechat.com/"
    id="embed-live168"
    data-id="62639938803d89447dfe45fc"
    src="https://app.168livechat.com/embed/template/index.js"
  >&#x3C;/script>
  &#x3C;script>
    //init iframe live chat and pass some params (extras)
    window.Live168API.init({
      webId: "62639938803d89447dfe45fc",
      extras: {
        userId:"User id",
        vToken:" v token ",
        fullname: "Name of user",
        age: 21,
        more: "more and more",
      },
    });
  &#x3C;/script>
&#x3C;/html></code></pre>

#### In there:

* `extras.userId`: If you use user authentication, pass the user's userId in there.
* `extras.vToken`: To authenticate the userId, you need to call http request via 168chat to get the vToken ([see more](../product-guides/project/project-config/config-verification/e-user-verification.md)).

In addition, you can also transfer the data you need into the extras.

## Client Side Rendering

### ReactJS

#### Installation Package

Install with Npm

```bash
npm i react-168chat
```

or with Yarn

```bash
yarn add react-168chat
```

#### Import Package

Import and use the library's components at the project's ./App.js

{% code title="./App.js" %}
```javascript
import React168Chat from "react-168chat"

<React168Chat
  projectId="your project id"
  secret="secret key"
  extras="{}"
></React168Chat>
```
{% endcode %}

**In there:**

* `projectId`: the Id of your project, you can get it from [Link Embed Setting](../product-guides/project/project-config/link-embed.md)
* `secret`: the secret key of your project, get it from [Link Embed Setting](../product-guides/project/project-config/link-embed.md)

### ReactNative

#### Installation Package

Install with Npm

```bash
npm i react-native-168chat
```

or with Yarn

```bash
yarn add react-native-168chat
```

#### Import Package

Import and use the library's components at the project's ./App.js

{% code title="./App.js" %}
```javascript
import RN168Chat from "react-native-168chat"

<RN168Chat
  projectId="your project id"
  secret="secret key"
  extras="{}"
  onClose={}
  isVisiable={true}
></RN168Chat>
```
{% endcode %}

**In there:**

* `projectId`: the Id of your project, you can get it from [Link Embed Setting](../product-guides/project/project-config/link-embed.md)
* `secret`: the secret key of your project, get it from [Link Embed Setting](../product-guides/project/project-config/link-embed.md)

#### Cocos Creator

Coming soon...



