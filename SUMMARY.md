# Table of contents

## Overview

* [💡 Overview](README.md)
* [Getting Started](overview/getting-started.md)

## Product Guides

* [Project](product-guides/project/README.md)
  * [Create Project](product-guides/project/create-project.md)
  * [Project Config](product-guides/project/project-config/README.md)
    * [Basic Config](product-guides/project/project-config/basic-config.md)
    * [Show](product-guides/project/project-config/show.md)
    * [Sound/Announcement](product-guides/project/project-config/sound-announcement.md)
    * [Call Config](product-guides/project/project-config/call-config.md)
    * [Config verification](product-guides/project/project-config/config-verification/README.md)
      * [E-user Verification](product-guides/project/project-config/config-verification/e-user-verification.md)
    * [Link Embed](product-guides/project/project-config/link-embed.md)
* [Chatting](product-guides/chatting.md)
  * [Search](product-guides/chatting/search.md)
  * [Whisper](product-guides/chatting/whisper.md)
  * [Data Storage](product-guides/chatting/data-storage.md)
  * [Invite Customer Service](product-guides/chatting/invite-customer-service.md)
* [Type Customer](product-guides/type-customer/README.md)
  * [How to use](product-guides/type-customer/how-to-use.md)
  * [Create Type Customer](product-guides/type-customer/create-type-customer.md)
* [Member](product-guides/member/README.md)
  * [Add Member to Project](product-guides/member/add-member-to-project.md)
* [Role](product-guides/role/README.md)
  * [Add a role](product-guides/role/add-a-role.md)
* [Quick Chat](product-guides/quick-chat/README.md)
  * [How to use](product-guides/quick-chat/how-to-use.md)
  * [Create quick chat](product-guides/quick-chat/create-quick-chat.md)
* [Form](product-guides/form.md)
  * [How to use](product-guides/form/how-to-use.md)
  * [Create a form](product-guides/form/create-a-form.md)
* [Miss Chat](product-guides/miss-chat.md)
* [Ban](product-guides/ban/README.md)
  * [How to use](product-guides/ban/how-to-use.md)
* [Ticket](product-guides/ticket/README.md)
  * [Create a ticket](product-guides/ticket/create-a-ticket.md)
* [Chat history](product-guides/chat-history.md)
* [Announcement](product-guides/announcement.md)
  * [Create an announcement](product-guides/announcement/create-an-announcement.md)
* [Bot Chat](product-guides/bot-chat.md)
* [Short Code](product-guides/short-code/README.md)
  * [How to use](product-guides/short-code/how-to-use.md)
  * [Create a short code](product-guides/short-code/create-a-short-code.md)
* [Knowledge Base](product-guides/knowledge-base/README.md)
  * [How to use](product-guides/knowledge-base/how-to-use.md)
* [Form Structure](product-guides/form-structure.md)
* [User Setting](product-guides/user-setting.md)
* [Overview](product-guides/overview.md)
* [Variable](product-guides/variable.md)
* [Department](product-guides/department.md)

## Fundamentals

* [🛠 Getting set up](fundamentals/getting-set-up/README.md)
  * [📝 Setting permissions](fundamentals/getting-set-up/setting-permissions.md)
  * [🧑 Inviting Members](fundamentals/getting-set-up/inviting-members.md)

## Use Cases

* [🎨 For Designers](use-cases/for-designers.md)
* [🖥 For Developers](use-cases/for-developers.md)
