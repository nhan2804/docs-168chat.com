# 💡 Overview

**OVERVIEW 168CHAT.COM**

**A. MỤC ĐÍCH**

* Cung cấp công cụ chăm sóc khách hàng sẵn có cho admin cài đặt vào website cá nhân & doanh nghiệp**.**
* Cung cấp môi trường và quy trình hỗ trợ giải đáp trực tuyến khách hàng truy cập website.
* Cung cấp công cụ theo giỏi, thống kê báo cáo & tầm soát được hiện trạng khách hàng truy cập website.

**B. TÍNH NĂNG**

1. **End-User**
   1. Chat trực tiếp với CS
   2. Chat với CS qua zalo, fb, telegram
   3. Hàng chờ khi CS full
   4. Xem knowledge base
   5. Để lại tin Offline
   6. Nhập pre-input
   7. Gửi media khi chat
   8. Điền form khi chat
   9. Ẩn chat
   10. Kết thúc chat
   11. Giao diện theo ngôn ngữ
2. **CS**
   1. Chọn trạng thái làm việc (working | rảnh tay) cho từng project
   2. Xem danh sách khách trong hàng chờ
   3. Tiếp nhận trực tiếp khách khi CS đang trong chế độ working của dự án
   4. Chọn chế độ hiển thị chat (single 1 khách | multi nhiều khách)
   5. Lấy khách từ hàng chờ để chat
   6. Xem thông tin chi tiết khách (Device, Location, Url, verify, Information ….)
   7. Mời cs khác vào hỗ trợ chat chung
   8. Chuyển tiếp khách cho CS có chuyên môn tiếp nhận
   9. Tạo vé chờ cho các yêu cầu cần thời gian giải quết cho khách hàng
   10. Xem trước nội dung chat của khách hàng để tối ưu thời gian phản hồi
   11. Tạo chat nhanh giúp việc tự động hóa trả lời các câu hỏi có sẵn
   12. Gửi form theo mẫu cho khách hàng ghi nhận
   13. Kho lưu trữ hình ảnh, form ghi nhận từ khách hàng cho việc tìm hỗ trợ khách hàng cũ hiệu quả hoặc CS mới
   14. Tìm kiếm nội dung chat cũ
   15. Gửi media, link, amination, gift-code, gift-packages … cho khách hàng tham khảo
   16. Lọc chặn khách hàng spam
   17. Tùy chỉnh bộ âm thanh chat
   18. Cơ chế thông báo khách hàng treo máy & tự động cập nhật trạng thái khách hàng
   19. Cơ chế thông báo cho khách hàng CS đi vắng
   20. Tùy chọn đã ngôn ngữ
3. **Admin**
   1. Tạo script cho website cá nhân/ doanh nghiệp
   2. Cá nhân hóa tool hiển thị trên website
   3. Tùy biến câu chào khách hàng
   4. Tùy chọn phong cách hiển thị từ kho theme phong phú (Yêu cầu theme theo phong cách của cá nhân website)
   5. Quản lý số lượng khách hàng tiếp nhận cho từng CS
   6. Quản lý lượng khách vào website
   7. Quản lý các CS đang hoạt động
   8. Quản lý vé tạo cho khách hàng
   9. Tùy chỉnh gift-code, gift-package, thông điệp chăm sóc khách hàng
   10. Xem lịch sử chăm sóc khách hàng
   11. Thống kê chất lượng CS & các cuộc thoại
   12. Quản lý knowledge base
   13. Quản lý shortcode
   14. Quản lý biến
4. **Saler**
   1. Quản lý chăm sóc website cho cá nhân/doanh nghiệp tham gia dùng tool 168chat
   2. Tùy biến các chức năng nâng cao cho tool
   3. Tư vấn & theo gỏi chi phí theo từng website
   4. Xem thống kê tình hình theo từng website
   5. Xem & xuất hóa đơn

**C. NỘI DUNG**

I. End-User

1. Chat trực tiếp với CS

Nhấp vào Icon Chat tại góc dưới cùng phía bên phải tại giao diện trang web đã được nhúng 168 Chat.![](.gitbook/assets/0)

Khi xuất hiện hộp thoại như bên dưới, chúng ta nhấp vào nút “Chat ngay với CS”.![](.gitbook/assets/1)

2\. Chat với CS qua zalo,fb,telegram

Bên dưới hộp thoại Chat sẽ có các nút Chat với CS thông qua Facebook, Zalo, Viber, Telegram, Skype.

![](.gitbook/assets/2)

3\. Hàng chờ khi CS full

Sau khi nhấp vào nút “Chat ngay với Cs”, nếu không có CS nào đang làm việc trên dự án hoặc hàng chờ đang đầy thì chúng ta sẽ được đưa vào hàng chờ.

![](.gitbook/assets/3)

4\. Xem knowledgebase

Trên hộp thoại Chat, nhấp vào icon kính lúp trên thanh chức năng. ![](.gitbook/assets/4)

Hộp thoại KnowledgeBase sẽ xuất hiện ngay sau đó.

![](.gitbook/assets/image\_2022-11-23\_134213437.png)

5\. Để lại tin offline (Cài đặt tại trang Admin)

Khi đang trong hàng chờ, sẽ có nút “Để lại tin nhắn”.![](.gitbook/assets/6)

Nhấp vào đó sẽ xuất hiện hộp thoại để lại tin nhắn.

![](.gitbook/assets/7)

6\. Nhập pre-input (cài đặt tại trang Admin)

Sau khi nhấp vào nút “Chat ngay với Cs” sẽ xuất hiện hộp thoại nhập “Pre-input”.

![](.gitbook/assets/8)

7\. Gửi media khi chat

Nhấp vào Icon Attachment trên thanh công cụ, sau đó lựa chọn tệp cần gửi và nhấn gửi tin nhắn.

![](.gitbook/assets/9)

8\. Điền form khi chat

Nhấp vào “Nhập” trên form, sau đó hộp thoại nhập form sẽ xuất hiện.![](.gitbook/assets/10) ![](.gitbook/assets/11)

9\. Ẩn chat

Tại hộp thoại Chat, nhấp vào Icon mũi tên ở góc phải phía trên, sẽ xuất hiện các lựa chọn, nhấp vào “Thu nhỏ” để ẩn Chat.

![](.gitbook/assets/12) ![](.gitbook/assets/13)

10\. Kết thúc chat

Tại hộp thoại Chat, nhấp vào Icon mũi tên ở góc phải phía trên, sẽ xuất hiện các lựa chọn, nhấp vào “Kết thúc chat” để kết thúc chat.

![](.gitbook/assets/14) ![](.gitbook/assets/15)

11\. Giao diện theo ngôn ngữ

II.CS

1.Chọn trạng thái làm việc (working | rảnh tay) cho từng project

Nhấp vào ảnh đại diện tại góc phải phía trên, sau đó nhấp vào “Trạng thái dự án” và cài đặt trạng thái làm việc.

![](.gitbook/assets/16) ![](.gitbook/assets/17)

2.Xem danh sách khách trong hang chờ

Tại giao diện dự án, mục Chatting. Hàng chờ sẽ xuất hiện ở ô bên trái.

![](.gitbook/assets/18)

3.Tiếp nhận trực tiếp khách khi CS đang trong chế độ working của dự án

Khi bật chức năng “Đang làm việc” tại dự án. Nếu có khách hàng thì khách hàng sẽ được đưa vào chat trực tiếp.

4.Chọn chế độ hiển thị chat (single 1 khách | multi nhiều khách)

Tại giao diện dự án, mục Chatting. Nhấp vào nút “Chức năng” sẽ xuất hiện lựa chọn chế độ hiển thị Chat.![](.gitbook/assets/19)

5.Lấy khách từ hàng chờ để chat

[Video hướng dẫn](https://drive.google.com/file/d/1YeWbWBZKHgCp2OuGkyzedTqIZULZZ3E-/view?usp=sharing)

Để lấy khách từ hàng chờ để chat, tại giao diện hang chờ. Lựa chọn khách sau đó nhấp vào dấu tích (✓) để lấy khách.

![](.gitbook/assets/20)

6.Xem thông tin chi tiết khách (Device, Location, Url, verify, Information ….)&#x20;

Tại giao diện Chat với khách hàng, phía trên đoạn chat sẽ có đầy đủ các thông tin chi tiết của khách hàng.

<figure><img src="https://lh6.googleusercontent.com/-Bv1hiD35D3t7wHrupUXUQbGcmtK8kzC19an7I_QTFIormE5BjH2yGaSgV7XWxQp02aE1pqeKj0q6A8yd1r6A7tKQAV6ammJHxB2an8gkN6QlaZvBf_TWby3OaoTu-mm5bMp4WBl3u_SnLlhTbFRDQW1j1cM5zgVyjpABmuZn6uxHI3VYFIDop5ql1hpn-aVwFqtBxhu8w" alt=""><figcaption></figcaption></figure>

7.Mời cs khác vào hỗ trợ chat chung

[Video hướng dẫn](https://drive.google.com/file/d/1N2WW4-C3SYu6jbMuP6m38dtNhVOfEGg1/view?usp=sharing)

Tại giao diện Chat với khách hàng, chọn Icon “Mời chăm sóc khách hàng” trên thanh chức năng.

<figure><img src="https://lh6.googleusercontent.com/PifUfkOZRNXBhuSQlj5i6rFTeggcGdgqZZk2JwdLrCxGGKxMuOmUq09ktOQ-kBuCYqKroJz7-PAFf4uwBh_tmZZfUfjdDKtnNJfSGYZIoLZxBnfjhvU51W6XT1G3NRzrArqeQXP2wnUOgoKuHyzmuz1XsOylN5qI-E5lwVDSt6-JlE9yrxSfc-ewvQABgcG2rMcadTueNQ" alt=""><figcaption></figcaption></figure>

Sau đó lựa chọn CS và nhấp “Mời” để mời cs vào hỗ trợ chat chung.&#x20;

<figure><img src="https://lh3.googleusercontent.com/Ip9NsSkIGFwmy5VDlsZ5NXSB0pemCSxDGBi38aCdKkZJbdLfFYqsux2eq_c-FlU0FbFLWSUyQO-u0fxt2umCkC9pzROZq4J4b9yybuHeC1PkssyNIxeD6j3mo_azw2fgxoCqnI4sz76xoAe2hgxUOnCVkKd9_v9Tk8pxgaqvqwrlIH2RhMVxogi8oW4sOlse5j900DsMhw" alt=""><figcaption></figcaption></figure>

8.Chuyển tiếp khách cho CS có chuyên môn tiếp nhận

[Video hướng dẫn](https://drive.google.com/file/d/18ndejViPQhhI6o353-OrENh6biZ3120j/view?usp=sharing)

Tại giao diện Chat với khách hàng, chọn Icon “Mời chăm sóc khách hàng”.

<figure><img src="https://lh6.googleusercontent.com/PifUfkOZRNXBhuSQlj5i6rFTeggcGdgqZZk2JwdLrCxGGKxMuOmUq09ktOQ-kBuCYqKroJz7-PAFf4uwBh_tmZZfUfjdDKtnNJfSGYZIoLZxBnfjhvU51W6XT1G3NRzrArqeQXP2wnUOgoKuHyzmuz1XsOylN5qI-E5lwVDSt6-JlE9yrxSfc-ewvQABgcG2rMcadTueNQ" alt=""><figcaption></figcaption></figure>

Sau đó lựa chọn CS và nhấp vào “Transfer” để chuyển tiếp khách cho CS khác.&#x20;

<figure><img src="https://lh5.googleusercontent.com/xCxt42ANlOGzX5_c3gh47eFXKW-dx5akaEQG8SxGOmag02VO5CHp-tldgaivG00P9nx7Pgr6GNSdNGp6B7AsZNhycUM3XvfXqoctIOn3CFjD4nk3f1dedNs4FCMSUS-GAT34uZd68LAIK8_jnqDKIydWr_Ds3IKta0DjgVzqETwLPAHgZ7vJ4Y3CFtUJCF8c3LtXiUQkqw" alt=""><figcaption></figcaption></figure>

9.Tạo vé chờ cho các yêu cầu cần thời gian giải quyết cho khách hàng

[Video hướng dẫn](https://drive.google.com/file/d/1YyOJwVKOn8LuVta5MfcAeS8iFLtMLrGv/view?usp=sharing)

Tại giao diện Chat với khách hàng, nhấp vào Icon “Vé” trên thanh chức năng.

<figure><img src="https://lh5.googleusercontent.com/AenZlOCD-sej1WZargQ8NJz-sx3oVtLR8FPcrr0ftj81C9S1XD7Dg7-2m1AIrCX7-kX-YnL3gytcoW0nCRY8bSlpIOBN05fyzGjXSo7LXJNmdR-0uDk3mdo3X0ciffIFFhbox31KB4LRImvFPb1SGVGyEQN_bAc7sCqPzmRjC-01A6poViDbsQysKt40oVCv_BwkvPdwYQ" alt=""><figcaption></figcaption></figure>

Điền các thông tin và nhấp vào nút “Tạo” để tạo vé.

<figure><img src="https://lh6.googleusercontent.com/4ugDh25mbFDHYU5eOcTvdp3v3IC0XEg2bEoiqrF93Kf5nR0A8fp6_9Xmgosxlc-Cy3YHRSrs9ibeSqnywC5cF2uFARhF6BNWTUCYjmKt8EffwpwNQ7OYqe_w0x9PgYwSIfmiP2IBCjbX2PSYo4AW9dUEInSiaP0cMf06SOC18xjS2IZ_fOQsxUQcAHxU2Pf1ZW7ycQWUxQ" alt=""><figcaption></figcaption></figure>

10.Xem trước nội dung chat của khách hàng để tối ưu thời gian phản hồi

11.Tạo chat nhanh giúp việc tự động hóa trả lời các câu hỏi có sẵn

[Video hướng dẫn](https://drive.google.com/file/d/1\_Sxn-cXCMZqglMePYujniW7KBOSF2Uko/view?usp=sharing)

Tại giao diện trang web, nhấp vào ảnh đại diện. Sau đó nhấp vào “Tin nhắn nhanh”.

<figure><img src="https://lh4.googleusercontent.com/WaRKUM2VFQkChuv0jFcaF8GOPAxIdEoiTDPof58xOBBnjmGAoRiqeMJ5wM4x_OVzk2KXDopRHhMV9_HxgVSNxPQNRLMq9CVineKyJ5FOkDd0PkdqlkEcjITqL99xAFqlqe0MdkTyWtdpsEe-OrCexYhbyoopOq5mFQYIx2-YEujIu0jnW1qH85wMVhnj5bF5uRfMK4r7ng" alt=""><figcaption></figcaption></figure>

Các chức năng của tin nhắn nhanh sẽ xuất hiện ở đây.

<figure><img src="https://lh6.googleusercontent.com/3n-H7fHASX0u1b0w8ewqp0LrvmB7eG1frW91Z-y0GmoHIZ86Nf0c-gUjuXkNj6Eh8fcKCZIJDDRw1CEbci1oDTpJJ__Oya-ZtJx8PYRvkJFV3nV5rxCI_8NBAVcdDEZxsnV742BLFOa5AXgJ2GBOYEVj0qHUy3oX4TMEoZAgQCJJfSonilPPNpEvN0gzAwjP2IqkHd14rQ" alt=""><figcaption></figcaption></figure>

12.Gửi form theo mẫu cho khách hàng ghi nhận

[Video hướng dẫn](https://drive.google.com/file/d/1F\_XjyFhFuu5qj313gteQlKwCnh8rL64\_/view?usp=sharing)

Tại giao diện Chat với khách hàng, chọn Icon “Gửi Form/Mẫu” trên thanh chức năng.&#x20;

<figure><img src="https://lh4.googleusercontent.com/txgt_o3NxYVCOcEsrPB54-Lmp-42koYMG0iqNIpc78kNee4ydnB0UyZIOu_ggv8VAwFvHi1V-trSamZtXkWvAvnpDiB5Bz1VgrtolNDOlCH7wn68SrFA9eWyvT91dkkbZ_6KlUJ7N37Nzw9UJH0txYKWCx0mryRT6jsFfaTy916LBjYSJvGebvEEDFXYgpD8H8mM_EO8CQ" alt=""><figcaption></figcaption></figure>

Sau đó nhấp vào Form muốn khách hàng nhập.

<figure><img src="https://lh5.googleusercontent.com/4ebjCZ8wOhfna8S2Qdp6sTEo9-5p0b_LDJ-X6pPdPccKtpv-K9vl_MgBjDzSVXYGXTkIOU8rI9AypefS3BMelcUB-bFOGJhOTD-jIOyKfz3j_Mjmpc9s9a5a5LLha7vZEAOVkxi5Y9fIm_UiePEcC0FAyFND4B7XBGrXBFoYYuhy2ONXJd_lNdl_GmMpgwohfejs2bDfWw" alt=""><figcaption></figcaption></figure>

13.Kho lưu trữ hình ảnh, form ghi nhận từ khách hàng cho việc tìm hỗ trợ khách hàng cũ hiệu quả hoặc CS mới

[Video hướng dẫn](https://drive.google.com/file/d/1r2IJHs6SmJW6gJPcFcpnyrvoXzFQuBtK/view?usp=sharing)

Tại giao diện dự án, mục Chatting. Nhấp vào “Kho dữ liệu”.

<figure><img src="https://lh6.googleusercontent.com/KghHg3pslbQhy2m3PlKPxiBMS-g8Vy7sBYTIMx0660jGP4wUpsf9xy9C5xnYETl8C8QlM6XaEaixTOeHYEfqNROj_wgnzoN79VzQUih4-79a-_7J3BUA-AwjgUTJkRlllFH0ACZa3EXvSVswdZunAtWMJ0xMpPoOYpl_3SvWiXDdmGhB78_AuDs1wtCmOEeL" alt=""><figcaption></figcaption></figure>

<figure><img src="https://lh4.googleusercontent.com/T3YRi5S3vZ18Qn4_WMZ65bEue8xcKcN7DV7iadVSyOagw373CAWewWvSWWLmiYuqwhdpZ2S_8fVVuc2e6zjNPBr8nzd9ojOm5hEbW2wFRSb2H663R7CyIsWB9mQbFnhv8P6sH95_FKInBdlGpn5iCCq44RBRxy-3iFH4xo1NwbCytwwQWUJ4_3pFKyUFhZZG" alt=""><figcaption></figcaption></figure>

14.Tìm kiếm nội dung chat cũ

Tại hộp thoại Chat, nhấp vào Icon Search trên thanh chức năng

<figure><img src="https://lh3.googleusercontent.com/hNpQHtURZ0MInWs5ZzDCdYMMH3OilTYPj5JEQF1xPf4xBu3KwWdHIr-A4bHY4z1SgFmYhb9ZEvMkDwWnDI1ecGRbN6htNWhf64FAFeIElzjn99UIa80RQtj0JtqaFMm8tVT1rhDzUvYoaunye-XLeMVjR_V4jtDKV0v9Dj2AqAZZex75PNCB1SHaFBxGGZUJ" alt=""><figcaption></figcaption></figure>

Sau đó nhập từ khóa cần tìm kiếm vào.

<figure><img src="https://lh6.googleusercontent.com/djTQUK5r3irHOxwVRmlWSs2SAI3_gdLeZI1pDLjMtGXpkPrJkzFy7whfiBZK4ig8KDYkhjm0EdS-tLecSAQ02f8IJyzdQovW5rv4mQ0iv47OnpoV1ZUfpPS9EdWBVW4RyhalJu3QRJ6RJ0pQsKQ-q54KSz2r4XnHFvQPfu0-0yklDSZJX4oQCB4fObzmbf-f" alt=""><figcaption></figcaption></figure>

15.Gửi media, link, amination, gift-code, gift-packages … cho khách hàng tham khảo

Tại hộp thoại Chat, nhấp vào Icon Attachment phía dưới bên phải. Chọn media cần gửi và nhấp Gửi.

<figure><img src="https://lh6.googleusercontent.com/4_Ylt2xsqtZq2woPvI0trLDMvx1pMQS6UJJj9QTA4oYAGk6nHbaXnfmvogYm1PQ86PzsgT-CtG1OKnHJZnj7iL0BbO92AAGgfWxGqCP-RqG-_Zr16ilm8it3icli8PPWBf930BniFg1oKxywGFy2v0iUXIuMdJ6V-73qB8uDxsWtuf9vOG5vNDJimjXq2POK" alt=""><figcaption></figcaption></figure>

16.Lọc chặn khách hàng spam

<figure><img src="https://lh6.googleusercontent.com/P2us3Cz38xErl7TvfK_b9yZVg7xrOv-6956pmnWsNx95B5fbW6smiuByfst__P5sc5IYOn5DCD3-MgVVSU5YNaVuJvyrreVa0ltMTLEgDJtoLI4cWsLxK4Gc4FKe9sBsuAlFdVWETa1Egj1apcmPSLHbPzbLTj-XmfWsH33Gv3qrwdLiKrbMN0D0EAXXri3Y" alt=""><figcaption></figcaption></figure>

Tại hộp thoại Chat, nhấp vào Icon Chặn khách hàng và nhấp “Chặn”.

17.Tùy chỉnh bộ âm thanh chat

<figure><img src="https://lh5.googleusercontent.com/GVwEK4v0QL6FSUx_jPDSTt5VEW6re5xS5P2brX5P3BLyaPq1W_OyFrQfoHdr_zvSGg0NHk168fSfZ3zvkFNPz6jV6-Sa0ZriRaoGhdYWqCBfE8AlNnD0B1Xq0DUsZ4qiy739SZJP99KJUsV3gFUcIEw5hKCc7MFNOfgHITKdDLKhwoidKvWxAdojiDctb8Ak" alt=""><figcaption></figcaption></figure>

Trong cấu hình dự án, chọn mục “Âm thanh/Thông báo”.

18.Cơ chế thông báo khách hàng treo máy & tự động cập nhật trạng thái khách hàng

19.Cơ chế thông báo cho khách hàng CS đi vắng

<figure><img src="https://lh4.googleusercontent.com/PPm4zr6yQTlcfY4GTcCiaE28fxYiJHpYBtlD_yPNykTg_qdUFXdC_2OyyXhSt4wujChKjqbrN0f3yQQqQsPg-1_VhzDr_htMfTg1CoBwKcpB_CnzR1RLcqXTlAqPZo4sahbxzy4dHeIOvW9l3KOouYiblePitT_k8whG5eU6SOSvNVNVJJSeOtjEoMXCixLA" alt=""><figcaption></figcaption></figure>

20.Tùy chọn ngôn ngữ

Tại trang web, nhấp vào Icon Language và chọn ngôn ngữ của bạn.

<figure><img src="https://lh6.googleusercontent.com/qHrb1OCXsy2CSRjKMYJekdI14iwq6Xq1QNcpJ7-xvXqYiIsREPFlqwuucpzw_jpxJ7nK6fS24qj1lrdyo2QRWHQy_IaJhBx6AQoXALZVYKnb-gDUJfQzZBXc7UHMQQ-7NOSejU_-_F-uI-vqtT01HvFI6NfxHncsXwkRNe9E7TUhyzD02ArQmAgGkWjfJwh9" alt=""><figcaption></figcaption></figure>

III. Admin

1.Tạo script cho website cá nhân/ doanh nghiệp

[Video hướng dẫn](https://drive.google.com/file/d/144SKRraM6Bd8ZXJ-Q5n0B6VNDsnImDOG/view?usp=sharing)

<figure><img src="https://lh6.googleusercontent.com/8FLcuM26ndnZ78gQswsJeMceRH4flYWykeU821dgV_dNkL4LcpyhKtUwl-gVDkgPkuhOc-Hy0s93Mb3-qz3_byltk6iuZSXBLtkXP6eD6x0HZbvkq-0WLLEwJrPwodGsCuH8CFYVsowkA0zvL0WY4vm-5SOWlUDzjs13tdyNnI5_DWnYwSdYrJ_ZV5qBKBxt" alt=""><figcaption></figcaption></figure>

Tại giao diện cấu hình dự án, chọn mục “Link nhúng”, các script để nhúng sẽ xuất hiện phía dưới.

2.Cá nhân hóa tool hiển thị trên website

Tại giao diện cấu hình dự án, chọn mục “Hiển thị”.

<figure><img src="https://lh5.googleusercontent.com/HifDfB4sw96nZHGDHMuJicf5j6b7tUBNLitGgF5BzgsjEg2fFHaD6FFlTd8jpG2-Yn7yJTGtYNCrQxQm3FkbvC5DZavqBpXtQ7JYdHU9CpImmSqbs4DiaDcaqZBB7fKaB0LSBWkAPJKFypoqTSgp2g4YfolgTkCCX7xs2v6iYL2Rrj2HIf0pTmNwUM4FMBtG" alt=""><figcaption></figcaption></figure>

3.Tùy biến câu chào khách hàng

Tại giao diện cấu hình dự án, chọn mục “Hiển thị”, và cấu hình “Câu thông báo chào” sau đó lưu lại.

<figure><img src="https://lh3.googleusercontent.com/0BiUOXjjBdWLsC_UQFA2qqU2wgIKNBq8Y17hPPQJKwQbYhHn5MJE6PeeZTwQwmhEVN_DCBaRpkp21ScmI9LHq9hoyABpgmtDzE_e7GuHEOi8czwAveD1o8vSZDc1hBH8xcuyBhbbe3kDsFqmT5hAEzz-MqsFksxxR1MxSsOnYd-CEA3ZOO4IK0tYHvOthlMv" alt=""><figcaption></figcaption></figure>

4.Tùy chọn phong cách hiển thị từ kho theme phong phú (Yêu cầu theme theo phong cách

của cá nhân website)

Tại giao diện cấu hình dự án, nhấp vào Theme. Giao diện theme sẽ xuất hiện ngay sau đó.

<figure><img src="https://lh4.googleusercontent.com/cgjBbqSQ5YM9JhlaF33fuxJgfmz4342rGGSdywQepfXJu2wqQIV6G6sRcvkTiMgYfzcISpo6UJydm2gHKmBgFXj12gDgexZoN93yID8Q8gX2P2pFTBlw2dAbCE0S9EvGv4r2VL7WKuUZrW27ufqBBppI3uU8lZYI9BN3ZAqTswd_9WSFKl5O3EWSgnbTKD0_" alt=""><figcaption></figcaption></figure>

<figure><img src="https://lh5.googleusercontent.com/hBzcMKz_gV9e_771mHUp1VJpfkpKsInQAgYbIWgltdowtDYT_g7J9vsRe59oG_DEGfazX56Gx1Gy7SAZ2bcucGSw87Dg8gkhFI1QRlTbUOYxm1rdtRNalt1ZgYV6LYa1az0GfpBAhcVaSf9RoefoKmgjIxHhefrqoCKTQWJwM4tiCnUwLXn8mv4w4MvHiR0E" alt=""><figcaption></figcaption></figure>

5.Quản lý số lượng khách hàng tiếp nhận cho từng CS

Tại giao diện cài đặt dự án, nhấp vào mục “Cấu hình cuộc gọi”, cài đặt “Số lượng mỗi CS được phép nhận cuộc gọi” sẽ xuất hiện bên dưới.

<figure><img src="https://lh5.googleusercontent.com/I8dkKncSOapZrfaFbsHUDX0-3S7i5PVhhZVf8vlfYJQCvSCnvR50kXzyzERL4rlIqypx-a7bxXGv1DAuZWsp-wy9cawLF3-UqPCaM4DJJ2bRntn7bV8AOM3EtGbHYto9NNKfhN04Ij-be_taNYzef8MPbvMP0j1qSHttjZxCW2mQw5OebeGdWK2bNnGAsnlw" alt=""><figcaption></figcaption></figure>

6.Quản lý lượng khách vào website

Tại giao diện dự án, nhấp vào tab “Khách hàng trực tuyến”, giao diện quản lý khách sẽ xuất hiện.

<figure><img src="https://lh5.googleusercontent.com/7WBbUpVCKZuFgyLvSfBIP380gf67ekjjCAEmVbFa0YLJgqJbaD-2G9TiVpIm21ebTXgqePzTEfq3FZikW79InwUg3LXbNk-LYSFZpTaJ70kkRkuFb6tm_reTE9Qc1Djx2J7EfHaaHcUTlbYe1h0vyBaH-MKpfU47qFbnwu5sxuB27cPMIKdXwwNCFOLasi-9" alt=""><figcaption></figcaption></figure>

7.Quản lý các CS đang hoạt động

Tại giao diện dự án, chọn tab “Agents”, giao diện quản lý các CS sẽ xuất hiện ngay sau đó.&#x20;

<figure><img src="https://lh5.googleusercontent.com/r0b4j2nedDrWLRPqBOHOQ4Kc4saYgBKj2XuaXfRdsqtOPTNGpuxAgdLdS_EvertvyjmT-eB0BrlY08CIJtOaqtSOs7EVgXtgg4zRpFGdza-2mAMpJVvXfTJW_Z6M60OdzyhuSPJYjSBCaxOBWxXfQeqwouhWpehijJh_5R3z4i_TdBi7L37Qp6kij83tbSkO" alt=""><figcaption></figcaption></figure>

8.Quản lý vé tạo cho khách hàng

Tại giao diện dự án, chọn tab “Vé/Ticket”, giao diện quản lý vé tạo sẽ xuất hiện ngay sau đó.&#x20;

<figure><img src="https://lh3.googleusercontent.com/8ByMvKQWcz333eSH7cG-LLvRsuAo-S6w7zYwwPkUN7lTmKNC6KjUWGm_teD1jNBIzrxuDvE8XYdLQySogNFbh__lKPhX9EeU4MpTiJeaAtFysn3M7kRjMP4edSzZUy_tmM75pK62Ei2ajmnuAhciVS_fYWmxYk2dAtbtjIkAZY5Jnrfv_O8rx7ccWhupRApq" alt=""><figcaption></figcaption></figure>

9.Tùy chỉnh gift-code, gift-package, thông điệp chăm sóc khách hàng

Tại giao diện dự án, sử dụng phím tắt Ctrl + S hoặc nhấp vào ICON cài đặt, sau đó chọn mục “Thông báo”

<figure><img src="https://lh4.googleusercontent.com/8gjqzjLJ0dNgBbk-Z6HlL__iTCEYReegCjaOa4wTVB2iayKoClCnzIahxpBhfjKoByBhurkG1VamDrJS8ojDO3liCSmeNd-s98R4boWOjHVTv69FdHqIv1X-fs-uR7wZhOzV6xOwcI13toSQ46sKekIpnBepvSnUpJIU_u-bCCW2_EAoFSnWKn50yd1TzgrM" alt=""><figcaption></figcaption></figure>

Giao diện quản lý thông báo sự kiện sẽ xuất hiện sau đó.

<figure><img src="https://lh4.googleusercontent.com/P60FVqDEn1KFYzaXtcIH2hAHLZZBNzXEEoijHIp9eTLUxBU7TQtp0mJDKsdxy-b4R4-Eydpaxx8XyW1SW-qj8CCOwSqzwAI8ZswPaqi9rC8F2_7fvnx4igS52moK2mQuO3jx_CAU1XyWFTD2IkMUJX2_o1Bc3IQ9aQs1Kg5snqNoEYGGsXwG7iSNaY47vJua" alt=""><figcaption></figcaption></figure>

10.Xem lịch sử chăm sóc khách hàng

Tại giao diện dự án, sử dụng phím tắt Ctrl + S hoặc nhấp vào ICON cài đặt trên thanh điều hướng, sau đó chọn mục “Lịch sử chat”

<figure><img src="https://lh6.googleusercontent.com/k1fWvlspFEpfFEGKJpZhd_5B3smkTNDrGrHTOnvTE5L7oTMOlucGT8PurhjCh8QS-dohwgUqoqHMdyk0Qn7eF61airs7XQMSEcncokrQIvBwzrkATqhbXpI9ec52FA697pVuiwKTYBXLOz_Q0ILwi5h788iYm3uEDeGsFgbFDh2Jb92-8GIyjv-ycK6JlBpC" alt=""><figcaption></figcaption></figure>

Giao diện lịch sử chăm sóc khách hàng sẽ xuất hiện sau đó.

<figure><img src="https://lh3.googleusercontent.com/pKAgxxCuUJYObwQ1ueHRrkoDX2_Sd5Kt2IdlIwPgTHpRftGw0KrFnlXfgpxxwc_Fp0qPz419R1SdrFqLGLMJZaba0kzuYMkqQBowu88UXyMPtbR50ahTpCVi-CqwHCMXrlNwAftqI1AhuSQI2jk9igLdaNTewOLK1fHFC90iyUWuxhr4u0Q5Dy8WkZ9egtOt" alt=""><figcaption></figcaption></figure>

11.Thống kê chất lượng CS & các cuộc thoại

Tại giao diện dự án, sử dụng phím tắt Ctrl + S hoặc nhấp vào ICON cài đặt trên thanh điều hướng, sau đó chọn mục “Thống kê”

<figure><img src="https://lh5.googleusercontent.com/nam3MYawi8dsFLjKF8yxVDCW0td55fW8zkKQyNJbA0ja8NiXkyVGtlyUha86oPF6iJw3ds3RBLjAT9Z4h6W9XQggY7CNhJK1zdEzcxdULpKKPag2TBQpIhKK2q9VuM9Fk8ws0NMvObW5nMlH2c4UI1zGIgQsBS7IW21pG64cy0DV5w-sWN6vlRDNcj-ESAWy" alt=""><figcaption></figcaption></figure>

Giao diện thống kê sẽ xuất hiện ngay sau đó.

<figure><img src="https://lh5.googleusercontent.com/iPezSTmWmZv-93fIoAKL04C-Fl12mFGm7Wom_u8xoH28VseJ-UQQsxVziavek5WzYZpPThJOTmPmRSA_a9Evi1zUOKqpMELGOZctqWR_8RMTVQz-F8_ukU8uFC2fKGrUZlZMY-riammxWlqvcXSiPoRLVoZdl-CA78ktKWHC2wLUepa3NXngXqb8xJapddM9" alt=""><figcaption></figcaption></figure>

12.Quản lý knowledgebase

Tại giao diện dự án, sử dụng phím tắt Ctrl + S hoặc nhấp vào ICON cài đặt trên thanh điều hướng, sau đó chọn mục “Knowledge Base”

<figure><img src="https://lh3.googleusercontent.com/4XSLGPt80a0JjvdCfztMf3RVWQnY08n318c382GZZBMfd5vS44MDSqGaUQZx8yJO_-f9RdVZRrPHj4Xlc9o4K9xCf2y3o1ygKOmIhlFEvQ78vJ9qMNxYfpAgiimIhr0uVtUMvGM5aP4hS5RsQxuE9KvzQOMbOAqVSQCTNUgOyD4ISd_hOGUkiNO2Y8UmXQHr" alt=""><figcaption></figcaption></figure>

Giao diện quản lý Knowledge Base sẽ xuất hiện ngay sau đó.

<figure><img src="https://lh4.googleusercontent.com/3gRvp2_JfoqYazZz1gHcSaYwhKxktwzvTjKXf_3iOwIXsQ07omRETI81LfC-TBmFszceejRgLkeLmqAoXwrufGc_8IE6jkvv1zgZZ9MUsHd2-eJdwrzG9g3kPN0lXpCauVFHHZLSFodiDmqXUqQ6bfhz414bfHxkHHscR1CaVURTKZ4CSBiHaqQfIHkc5Z5r" alt=""><figcaption></figcaption></figure>

13.Quản lý shortcode

Tại giao diện dự án, sử dụng phím tắt Ctrl + S hoặc nhấp vào ICON cài đặt trên thanh điều hướng, sau đó chọn mục “Short code”

<figure><img src="https://lh4.googleusercontent.com/GWcTsd-0g0Gf8S5t6yZP-aW5EDjOf1oX5doKpSXt5w_U5c6QDgC9eB_isNp8ijuzq6vcOTiBKnoaZAFLkBdTdOl4JPd9SPcTkI3KinmW37K499j9H1GCctxM4F3WlDnXRIHhSpQvnjsXdg-oTxEIPMU2k07WT6tPUXAvPojzazQQKVC0VCqxxlTDIRH58HQu" alt=""><figcaption></figcaption></figure>

Giao diện quản lý short code sẽ xuất hiện ngay sau đó.

<figure><img src="https://lh4.googleusercontent.com/mWM1n9lVTinH3TmJ3j4hIbXzJSJnnQXHsJV09fRFMNK8OcLCDFGAih5x7NlnfInTVWsZib12lMCpjsnMMsnMbK7LYVSsuKqSJXmPGYTAFvpHqqhfIbBZWZmlIanaiylt_EJc_XJvhrAOE77-zn9Ua4R2suQj1iBm0TZ5Be3P4LXoEQM1hrR9xuB1KOZymIun" alt=""><figcaption></figcaption></figure>

14.Quản lý biến

Tại giao diện dự án, sử dụng phím tắt Ctrl + S hoặc nhấp vào ICON cài đặt trên thanh điều hướng, sau đó chọn mục “Biến”

<figure><img src="https://lh4.googleusercontent.com/tuSn3EKqox-0ldKXh3w8xNMWwGoZxAteZnTThZbWYugYYzSyIYlRrXyblIcI4pFYsK91MjnV3JhRoZ5_yUZVezETTYwRVxXaXmRO8ZJs8JokKFtEI_ntuBP-m2g4mveDN_JigQ7sPHmvhWpc9Rw-9DvF6PfkjNT4-6roQsWq9dXdvRQT7gsGo4-SD_VFg59p" alt=""><figcaption></figcaption></figure>

Giao diện quản lý biến sẽ xuất hiện ngay sau đó.

<figure><img src="https://lh5.googleusercontent.com/3XfP_Z2ncJpWJZxY77L03LTmceYpsXP52D3G9GljtRjYoQAriW2Ze0M4GUDJmbcDRXV3tWAwAfpF6-8Y7TVtXiCkJ_AUm-4i1maZoWCoFJgzalL-MnVUO1EYTc_cKMiQRKahU73-U1gOqq0H_GpC5Bba2sb5Icyzjh13OQDKgo85MisTZHa8x2mTir8e1R_D" alt=""><figcaption></figcaption></figure>

IV.Sale

1.Quản lý chăm sóc website cho cá nhân/doanh nghiệp tham gia dùng tool 168chat

2.Tùy biến các chức năng nâng cao cho tool

3.Tư vấn & theo dõi chi phí theo từng website

4.Xem thống kê tình hình theo từng website

5.Xem & xuất hóa đơn
