# Chatting

This is the chat interface of the project

<figure><img src="../.gitbook/assets/image (20).png" alt=""><figcaption><p>Chat interface</p></figcaption></figure>

### The queue, chat room

**Queue:** This is the customer's queue, if the agent sets **Working Mode** at the [Project Status](user-setting.md#project-status), the customer will be able to chat directly without having to enter the queue, otherwise the customer will enter the queue.

**Chat room:** this is the list of active chats of the agent, you can set a limit on the number of chats the agent can have at a time in the [Call Config](project/project-config/call-config.md)



<figure><img src="../.gitbook/assets/image (9).png" alt=""><figcaption><p>Queue &#x26; Chat room</p></figcaption></figure>

### Toolbar, Customer Data

**Customer Data**: This will contain customer data, including: IP, Country, Location, [Pre-input data](project/project-config/show.md), [Extras data](project/project-config/link-embed.md),...

**Toolbar**: includes functions: [Quick Chat](quick-chat/how-to-use.md), [Form](form/how-to-use.md), Bot Chat, [Invite Customer Service](chatting/invite-customer-service.md), [Whisper](chatting/whisper.md), [Search](chatting/search.md), [Knowledge Base](knowledge-base/how-to-use.md), [Ticket](ticket/create-a-ticket.md), [Ban](ban/how-to-use.md), Send Media, Send Emoji,..

<figure><img src="../.gitbook/assets/image (39).png" alt=""><figcaption><p>Customer Data &#x26; Toolbar</p></figcaption></figure>

### Chat mode

This feature makes it possible to review the history of ended chats.

<figure><img src="../.gitbook/assets/image (19).png" alt=""><figcaption><p>Single mode</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (44).png" alt=""><figcaption><p>Multiple mode</p></figcaption></figure>
