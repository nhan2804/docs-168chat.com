# How to use

To block/ban a customer, you can do the following in the chat interface.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-17 (1).gif" alt=""><figcaption><p>Ban customer</p></figcaption></figure>

To unban simply click Unban. Or you can also visit the [Banned List](./#ban-management) to unban customers.

<figure><img src="../../.gitbook/assets/image (7) (1).png" alt=""><figcaption><p>Unban customer</p></figcaption></figure>
