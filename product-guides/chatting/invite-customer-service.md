# Invite Customer Service

### Invite to the conversation

Here are the steps to invite another agent to join the conversation.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-18.gif" alt=""><figcaption><p>Invite to conversation</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-18 (1).gif" alt=""><figcaption><p>Accept the invitation</p></figcaption></figure>

### Transfer chat

Here are the steps to transfer the conversation to another agent.

<figure><img src="../../.gitbook/assets/chrome-capture.gif" alt=""><figcaption><p>Transfer chat</p></figcaption></figure>

