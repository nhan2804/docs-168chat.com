# Search

This function helps you find messages in chats.

The steps are quite simple. Just click the Search button on the Toolbar then enter the keyword to search.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-18 (4).gif" alt=""><figcaption><p>Search in message</p></figcaption></figure>
