# Data Storage

This function will help you to save the necessary data and then be able to find them and view them. **** The types of data you can save include photos, videos, forms,...

**How to save data**

Here are the Steps to save the data

<div>

<figure><img src="../../.gitbook/assets/image (8).png" alt=""><figcaption><p>Step 1: Click on '...' in the message.</p></figcaption></figure>

 

<figure><img src="../../.gitbook/assets/image_2022-11-18_162601203.png" alt=""><figcaption><p>Step 2: Click on Save</p></figcaption></figure>

</div>

<figure><img src="../../.gitbook/assets/image (6).png" alt=""><figcaption><p>Step 3: Fill all data then click Save</p></figcaption></figure>

**How to view the saved data**

Here are the Steps to view the saved data

<figure><img src="../../.gitbook/assets/image (15).png" alt=""><figcaption><p>Step 1: Click on Data Storage in Chat Interface</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (35).png" alt=""><figcaption><p>Step 2: Click on Preview in the data that you want to view.</p></figcaption></figure>

