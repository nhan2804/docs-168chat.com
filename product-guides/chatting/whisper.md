# Whisper

This function will help agents talk to each other in the chat without the customer seeing those messages.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-18 (2).gif" alt=""><figcaption><p>Whisper feature</p></figcaption></figure>
