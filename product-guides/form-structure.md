# Form Structure

To use functions like Pre-input, Form,...You need to properly pass our json structure so that we can process it.



Its structure will look like this:

```json
#List of Form Item
[
  #Example Form Item
  {
    "type": "input",
    "label": "Full name",
    "name": "name",
    "placeholder": "Input your name"
  },
  {
    "type": "select",
    "label": "Region",
    "name": "region",
    "placeholder": "Select your region",
    "options": [
      {
        "name": "SEA",
        "value": "sea"
      },
      {
        "name": "NA",
        "value": "na"
      }
    ]
  },
]
```



In it, each Form Item will have the following fields:

* type: Type of form item, we accept the following value: `input`, `textarea`, `checkbox`, `radio`, `select`, `image`, `date`, `datetime`, `single-checkbox`, `number`.
* label: Label of form item.
* placeholder: Placeholder of form item.
* options: only used for form item types select, checkbox, radio, single-checkbox. Inside each option there will be a `label` and a `value`
* name: Name of form item.



