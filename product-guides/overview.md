# Overview

This feature will help you to see an overview of the project statistics.

<figure><img src="../.gitbook/assets/image (27).png" alt=""><figcaption><p>Sample Overview Page</p></figcaption></figure>

Here's how to get to the overview page.

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption><p>Step 1: Click on Setting Icon or Press 'Ctrl + S'<br></p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (3).png" alt=""><figcaption><p>Step 2: Click on Overview</p></figcaption></figure>

