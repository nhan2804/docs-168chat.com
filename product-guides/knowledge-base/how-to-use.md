# How to use

Knowledge Base is very handy, we will try to use it in the following ways.

### Agents view knowledge base

In [Knowledge Base Management](./#knowledge-base-management), click on the title of knowledge base that you want to view.

<figure><img src="../../.gitbook/assets/image (2) (2).png" alt=""><figcaption><p>Click on the title of knowledge base</p></figcaption></figure>

### Send a knowledge base to customers.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-17.gif" alt=""><figcaption><p>Send a knowledge base to customers</p></figcaption></figure>

