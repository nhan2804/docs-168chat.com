# Knowledge Base

This feature will help you create pre-made tutorials or knowledge, and then send them to customers for them to see the tutorial.

### Knowledge Base Management

Here are the steps to access the management interface

\


<figure><img src="../../.gitbook/assets/image (8) (1).png" alt=""><figcaption><p>Step 1: Click on Setting Icon</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (10).png" alt=""><figcaption><p>Step 2: Click on Knowledge Base</p></figcaption></figure>
