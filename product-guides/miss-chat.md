# Miss Chat

### Miss Chat Management

Here's the way to access the management interface

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption><p>Click on Tab Miss Chat in Project interface.</p></figcaption></figure>



This feature will include [Offline Message](miss-chat.md#offline-message) and [Miss Chat](miss-chat.md#undefined)

### Offline Message

Requires to enable **Offline Mode** and fill up the **Offline message input** in [Project Show Setting](project/project-config/show.md)

This feature will leave an offline message letting us know when the customer leaves before they reach the agent.

### Miss Chat

This function will leave the conversation when the customer has sent a message but does not receive an agent response.

