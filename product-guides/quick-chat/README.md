# Quick Chat

### Quick Chat Management

Here are the steps to access the management interface

<figure><img src="../../.gitbook/assets/image_2022-11-16_170740266.png" alt=""><figcaption><p>Click on Setting Icon</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-16_170758953.png" alt=""><figcaption><p>Click on Quick Chat</p></figcaption></figure>
