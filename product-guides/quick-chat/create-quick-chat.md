# Create quick chat

<figure><img src="../../.gitbook/assets/image_2022-11-16_170844632.png" alt=""><figcaption><p>Click on Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-16_170858199.png" alt=""><figcaption><p>Click Agree Button after entering all data</p></figcaption></figure>



### **Structure**

* Title**:** Title of quick chat, this will affect calling it to use (see [How to use](how-to-use.md))
* Type: Type of quick chat
* Content: Quick Chat content can contain [Short Code](../short-code/) using `[shortCodeName]`. In addition, you can also use variables like: `{{project_name}}`, `{{agent}}`, `{{userId}}` to replace the data.&#x20;
