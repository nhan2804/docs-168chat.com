# How to use

At the Chat interface, enter the "#" character to display the list of available Quick Chats. You can also write more to search for quick chat.&#x20;

<figure><img src="../../.gitbook/assets/image (21).png" alt=""><figcaption><p>Quick chat searching</p></figcaption></figure>

Then click on the Quick Chat you want to use, the content of that Quick Chat will be generated.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-16.gif" alt=""><figcaption><p>Select quick chat to use</p></figcaption></figure>
