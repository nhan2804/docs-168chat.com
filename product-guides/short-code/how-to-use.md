# How to use

Short Code can be used in many places such as direct message with customers, [Quick SMS](../user-setting.md#quick-sms), [Quick Chat](../quick-chat/create-quick-chat.md),...

To use it, simply use \[ShortCodeName] at the text you want to add it to.

<figure><img src="../../.gitbook/assets/chrome-capture-2022-10-19.gif" alt=""><figcaption><p>Use Sample Short Code in Conversation</p></figcaption></figure>
