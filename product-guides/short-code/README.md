# Short Code

This feature is made to be able to pre-write text, html or slider to be used quickly.

### Short Code Management

Here are the steps to access the management interface

<figure><img src="../../.gitbook/assets/image.png" alt=""><figcaption><p>Step 1: Click on Setting Icon</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (33).png" alt=""><figcaption><p>Step 2: Click on Short Code</p></figcaption></figure>

