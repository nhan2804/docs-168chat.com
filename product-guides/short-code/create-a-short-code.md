# Create a short code

At the [Short Code Management](./#short-code-management) interface, you perform the following steps.

<figure><img src="../../.gitbook/assets/image (7).png" alt=""><figcaption><p>Step 1: Click on Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (5).png" alt=""><figcaption><p>Step 2: Click the Create Button after filling all the data</p></figcaption></figure>



### **Structure**

* Name: The name of the short code.
* Description: The description of short code.
* Type: Currently there are 3 types.
  * Text: just plain text.
  * Regular HTML: is a piece of HTML code.
  * Slider: is a **JSON Slider** (see below)

#### JSON Slider

It is an array of json objects, containing 3 fields: `title`, `thumb`, `desc`

* title: title of an object in the slide
* thumb: thumbnail's link of an object in the slide&#x20;
* desc: description of an object in the slide

Sample Json Slider

```json
#List of item in slide
[
	#Sample item in slide
	{
		"thumb": "https://app.168livechat.com/assets/168Icon.9d7b2050.png",
		"title": "Item 1",
		"desc": "Description of item 1"
	},
	{
		"thumb": "https://app.168livechat.com/assets/168Icon.9d7b2050.png",
		"title": "Item 2",
		"desc": "Description of item 2"
	},
	{
		"thumb": "https://app.168livechat.com/assets/168Icon.9d7b2050.png",
		"title": "Item 3",
		"desc": "Description of item 3"
	}
]
```
