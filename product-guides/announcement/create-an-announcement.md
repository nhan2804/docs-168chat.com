# Create a announcement

To create an announcement, you navigate to the [Announcement Management](../announcement.md#announcement-management) interface. Then perform the following steps:

<figure><img src="../../.gitbook/assets/image (4) (2).png" alt=""><figcaption><p>Step 1: Click to Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (7) (2).png" alt=""><figcaption><p>Step 2: Fill all data then click Create</p></figcaption></figure>



### **Structure**

* Title: Title of announcement
* Content: Content of the announcement
* Effective time: start and end time of announcement
* Show: hide or show announcement
* Pin as maintenance: If you enable it, the client will receive this message as maintenance and cannot start chat.
