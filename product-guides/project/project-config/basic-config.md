# Basic Config



### Basic Config <a href="#cau-hinh-giao-dien" id="cau-hinh-giao-dien"></a>

<figure><img src="../../../.gitbook/assets/image_2022-11-21_133132197.png" alt=""><figcaption><p>Config Basic</p></figcaption></figure>

* `Name`: Name of project
* `Website URL`: Url of project
* `Description`: Description of project
* `Language`: Language to be displayed to the client (on chat iframe)
