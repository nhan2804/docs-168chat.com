# Link Embed

<figure><img src="../../../.gitbook/assets/image_2022-11-16_165603501.png" alt=""><figcaption><p>Link Embed</p></figcaption></figure>

* `Link Embed`: Embed code, used to embed it in your website
* `ID`: The id of your project
* `Secret key`: The secret key of your project
