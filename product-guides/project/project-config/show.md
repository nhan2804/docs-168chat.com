# Show



### Show <a href="#cau-hinh-nang-cao" id="cau-hinh-nang-cao"></a>

<figure><img src="../../../.gitbook/assets/image_2022-11-21_133038061.png" alt=""><figcaption><p>Show</p></figcaption></figure>

* <mark style="color:red;">`Hello message`</mark>:  Title of chat dialog
* `Description`: Description of chat dialog
*   `Theme`: Theme of chat dialog.

    * `Custom Theme`: There are many custom themes, you can check them out
    * `Primary Color`: Primary color of theme
    * `Secondary Color`: Secondary color of theme


* `Enter input before chatting`: [Form Structure](../../form-structure.md). The customer will fill out this form before starting the conversation.
* `Offline mode`: Turn off or turn on offline function (see more here [Offline Message](../../miss-chat.md#offline-message))
* `Offline message input`: [Form Structure](../../form-structure.md). This form is part of the offline message.
* `Variable`:&#x20;
* `Hello Message`:
* `Popup's position`: Location of Chat's popup
* `Background`: Background of chat dialog.
* `Custom Button`: Button of chat dialog.
* `Logo`: Logo of chat dialog.
