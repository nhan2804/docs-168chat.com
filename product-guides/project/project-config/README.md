# Project Config



### Project Config <a href="#cau-hinh-du-an" id="cau-hinh-du-an"></a>

Here's how to access the project settings.&#x20;

In the project interface, you perform the following steps.

<figure><img src="../../../.gitbook/assets/image_2022-11-16_165201846.png" alt=""><figcaption><p>Step 1: Click on Setting Icon</p></figcaption></figure>

<figure><img src="../../../.gitbook/assets/image_2022-11-16_170200729.png" alt=""><figcaption><p>Step 2: Click on Config</p></figcaption></figure>

There will be a lot of settings, see more manual pages of each installation section.



