# Sound/Announcement

<figure><img src="../../../.gitbook/assets/image_2022-11-16_165407245.png" alt=""><figcaption><p>Sound/Announcement</p></figcaption></figure>

You will be able to adjust the volume or sound of events such as:

* Incomming customer
* Invite join room
* Waiting
* Incomming chat
* New CS join room
* End chat
