# E-user Verification

At the [Verification Configuration Page](./), copy the `Token for e-user verification` (click Create Button if it is not already there)

<figure><img src="../../../../.gitbook/assets/image (1).png" alt=""><figcaption></figcaption></figure>

After getting the token, before every time you render the html for the user, make an http request to 168 chat to get the vToken for that user.

**Sample Request**:

\[<mark style="color:orange;">`POST`</mark>]

`https://168chat.com/api/projects/{projectId}/verify-token/encode`

\[<mark style="color:orange;">`BODY`</mark>]

```json
{
  "payload": {
    "userId": "user id" // optional
  },
  "verifyToken": "ZHcaLbzwo3lrt03kM7URw" the Token for e-user verification
}
```

Once you get the vToken, put it in extras.vToken, see more at [Getting Started](../../../../overview/getting-started.md) page

