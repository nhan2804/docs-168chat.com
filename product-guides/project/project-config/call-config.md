# Call Config

<figure><img src="../../../.gitbook/assets/image_2022-11-16_165425518.png" alt=""><figcaption><p>Call Config</p></figcaption></figure>

* `Number of each CS allowed to receive calls`: The maximum number of customers that Cs can chat at the same time.
* `Calling allowed`: Enable call feature
* `Chat on other platforms`: Allows displaying links to chat links on other platforms of cs
