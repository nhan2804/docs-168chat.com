# Create Project



### How to create a Project <a href="#tao-tu-an" id="tao-tu-an"></a>

Here's how to create a project. At the website interface, you perform the following steps.

<figure><img src="../../.gitbook/assets/image_2022-11-21_183520278.png" alt=""><figcaption><p>Step 1: Click on Create Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-21_183548266.png" alt=""><figcaption><p>Step 2: Click Create Button after entering all data</p></figcaption></figure>

### **Structure**

* `Logo` : Project's logo _(optional)_
* `Name` : App's name
* `Description` : Project's description
* `Website URL`: Url of your website \
  For example: `https://soundcloud.com/`,`https://serpapi.com/`



