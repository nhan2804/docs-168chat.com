# How to use

Click Form on the toolbar, then select the Form you want the customer to fill out. Customers will receive a request to fill out that form.

<figure><img src="../../.gitbook/assets/123.gif" alt=""><figcaption><p>Use the form</p></figcaption></figure>
