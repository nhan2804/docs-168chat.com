# Create a form

<figure><img src="../../.gitbook/assets/image (9) (2).png" alt=""><figcaption><p>Click on Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (13).png" alt=""><figcaption><p>Click Agree Button after entering all data</p></figcaption></figure>

### **Structure**

* Name: Name of the Form
* Content: The [Form Json Structure](../form-structure.md)&#x20;
