# Type Customer

This feature will help you to classifies the customers. For example: VIP Customers, Debt Customers,...

Other agents will also see how customers are classified.

### Type Customer Management

Here are the steps to access the management interface

<figure><img src="../../.gitbook/assets/image_2022-11-16_170347339.png" alt=""><figcaption><p>Click on Setting Icon</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-16_170410063.png" alt=""><figcaption><p>Click on Customer Type</p></figcaption></figure>

