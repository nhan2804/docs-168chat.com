# Create Type Customer

Here's how to create a type customer at the [Type Customer Management](./#type-customer-management) interface

<figure><img src="../../.gitbook/assets/image_2022-11-16_170448491.png" alt=""><figcaption><p>Step 1: Click on Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (31).png" alt=""><figcaption><p>Step 2: Click Create Button after entering all data</p></figcaption></figure>

### **Structure**

* Name: Name of Customer Type
* Description: Description of Customer Type
* Color: Color of Customer Type
