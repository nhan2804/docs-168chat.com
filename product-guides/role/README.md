# Role

This feature helps you to create decentralized classes. You can then use these classes to apply to the Agents in the project.

### Role Management

Here are the steps to access the management interface

<figure><img src="../../.gitbook/assets/image_2022-11-16_170602324.png" alt=""><figcaption><p>Click on Setting Icon</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-16_170623173.png" alt=""><figcaption><p>Click on Role</p></figcaption></figure>

