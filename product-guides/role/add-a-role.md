# Add a role

<figure><img src="../../.gitbook/assets/image_2022-11-16_170643034.png" alt=""><figcaption><p>Click on Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-16_170718501.png" alt=""><figcaption><p>Click Agree Button after entering all data</p></figcaption></figure>

### **Structure**

* Name**:** Name of Role
* Description: Description of Role
* Basic/Chatting: Permissions of Role
