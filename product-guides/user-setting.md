# User Setting

Here's how to view and set user settings.&#x20;

After clicking on your profile picture, an assortment of settings will appear.

<figure><img src="../.gitbook/assets/image (43).png" alt=""><figcaption><p>Click on Your Avatar</p></figcaption></figure>

### Edit Profile

You can set your name, phone number, link to social networking sites (this is related to [Social Chatting](project/project-config/call-config.md) section)

### Config Personal

<figure><img src="../.gitbook/assets/image (45).png" alt=""><figcaption><p>Config Personal</p></figcaption></figure>

Inactive room viewing time: How long to hide chat since chat ended (in minutes)

Reverse the position of the chat list:&#x20;

### Addon

Addon functions

### Project Status

This function will decide whether you will receive chat with customers immediately or just take customers from the queue.

There are 2 options here for each project you participate in:

* Working: When a customer initiates a conversation, you automatically accept to reply immediately.
* Away: Customers may enter a queue, at which point you will decide whether or not to take the conversation to reply.

<figure><img src="../.gitbook/assets/image (14).png" alt=""><figcaption><p>Sample Project Status</p></figcaption></figure>

### Quick SMS

This function is similar to [Quick Chat](quick-chat/how-to-use.md), but this is private, only you can use the Quick SMS you have created.

### Logout

Sign out of your account.



