# Form

This feature helps you create a Form and ask customers to enter that Form. You can then store the form that the customer received back and access it later.

### Form Management

Here are the steps to access the management interface

<figure><img src="../.gitbook/assets/image_2022-11-16_170740266.png" alt=""><figcaption><p>Click on Setting Icon</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (3) (2).png" alt=""><figcaption><p>Click on Form</p></figcaption></figure>
