# Announcement

### Announcement Management

Here are the steps to access the management interface

<figure><img src="../.gitbook/assets/image (5) (3).png" alt=""><figcaption><p>Step 1: Click on Setting Icon<br></p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (5) (2).png" alt=""><figcaption><p>Step 2: Click on Announcement in Advanced Section</p></figcaption></figure>
