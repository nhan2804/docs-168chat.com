# Chat history

This feature makes it possible to review the history of ended chats.

### Chat History Management

Here are the steps to access the management interface

<figure><img src="../.gitbook/assets/image (1) (2).png" alt=""><figcaption><p>Step 1: Click on Setting Icon or Press 'Ctrl + S'</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (5) (4).png" alt=""><figcaption><p>Step 2: Click on Chat History</p></figcaption></figure>
