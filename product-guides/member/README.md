# Member

This feature will help you to add Agents to your project.

### Member Management

Here are the steps to access the management interface

<figure><img src="../../.gitbook/assets/image_2022-11-16_165658028.png" alt=""><figcaption><p>​Click on Setting Icon​</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image_2022-11-16_172437871.png" alt=""><figcaption><p>Click to Member</p></figcaption></figure>
