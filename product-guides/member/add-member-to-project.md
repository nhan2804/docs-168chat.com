# Add Member to Project

Here's how to add Member to Project at the [Project Member Management](./#member-management)[ ](../type-customer/#type-customer-management)interface

<figure><img src="../../.gitbook/assets/image_2022-11-16_170526687.png" alt=""><figcaption><p>Step 1: Click on Add Button</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (4) (1).png" alt=""><figcaption><p>Step 2: Click Add Button after entering all data</p></figcaption></figure>

### **Structure**

* Email: Email of the user you want to add as a member
* Role: [Role ](../role/)of member
