# Create a ticket

There are 2 ways for you to create a Ticket.

1.  Create tickets based on chat rooms.



    <figure><img src="../../.gitbook/assets/image (12) (1).png" alt=""><figcaption><p>Step 1: On the Chat interface, click Ticket in the toolbar</p></figcaption></figure>



    <figure><img src="../../.gitbook/assets/image_2022-11-17_142752679.png" alt=""><figcaption><p>Step 2: Click Create Button after entering all data</p></figcaption></figure>
2.  Create a ticket at the [Ticket Management](./#ticket-management).



    <figure><img src="../../.gitbook/assets/image (8) (2).png" alt=""><figcaption><p>Click on Add Button in the <a href="./#ticket-management">Ticket Management</a></p></figcaption></figure>



<figure><img src="../../.gitbook/assets/image_2022-11-17_143449478.png" alt=""><figcaption><p>Step 2: Click Create Button after entering all data</p></figcaption></figure>

### **Structure**

* Name: Name of ticket
* Description: Description of ticket
* Priority: Priority of ticket
* Status: There are 2 states of ticket
  * Waiting: ticket is pending
  * Approve: ticket has been resolved
